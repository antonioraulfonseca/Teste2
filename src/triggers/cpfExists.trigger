trigger cpfExists on Cliente__c (before insert, after update) {
   
   Cliente__c[] clis;
   
   if(trigger.isInsert){
        clis = [Select Name from Cliente__c 
                where Name =: Trigger.new[0].Name] ;
   }else{
        clis = [Select Name from Cliente__c 
                where Name =: Trigger.new[0].Name
                And Id !=: Trigger.new[0].Id] ;
   }
    
     
    if(clis.size()!=0){
       
       Trigger.new[0].addError('Erro CPF Já existe!');
    }
}