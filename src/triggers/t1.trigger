trigger t1 on Book__c (before delete, after delete, after undelete){
    
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            if(p.firstValue){
                Trigger.old[0].addError('Before Book Delete Error');
                p.firstValue = false;
            }
        }
    }
}