trigger contaTrigger on Account (before insert,before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            new ContaTriggerHandler(Trigger.New);
        }
    }
}