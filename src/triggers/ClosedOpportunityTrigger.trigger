trigger ClosedOpportunityTrigger on Opportunity (before insert) {
List<Task> taskList = new List<Task>();
    for(Opportunity opp : Trigger.New){
        if(opp.StageName == 'Closed Won'){
            Task tarefa = new Task();
            tarefa.Subject = 'Follow Up Test Task';
            tarefa.ActivityDate = System.today().addMonths(1);
            tarefa.WhatId = opp.id;
            taskList.add(tarefa);
        }
    }
    insert taskList;
}