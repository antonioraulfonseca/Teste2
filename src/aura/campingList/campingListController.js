({
    doInit: function(component, event, helper) {
        
        var action = component.get("c.getItems");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.items", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        $A.enqueueAction(action);
    },    
    
    /*doInit : function(component,event,helper){
        var action = component.get("c.getItems");
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.items",response.getReturnValue());
            }else{
                console.log("Error: " + state);
            }
        });
        $A.enqueueAction(action);
    },
     clickCreateItem : function(component,event,helper){
        if(helper.validateFields(component)){
            helper.createItem(component,component.get("v.newItem"));
        }
    },
    handleAddItem : function(component,event,helper){
        var insertItem = event.getParam("item");
        var action = component.get("c.saveItem");
        action.setParams({
            "item": insertItem
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var listItems = component.get("v.items");
                listItems.push(response.getReturnValue());
                component.set("v.items", listItems);
            }
        });
        $A.enqueueAction(action);
    }*/
    handleAddItem: function(component, event, helper) {
        var item = event.getParam("item");
        
        var action = component.get("c.saveItem");
        //json stringify is not needed I think.       
        action.setParams({
            "item": item
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {        
                var items = component.get("v.items");
                items.push(item);
                component.set("v.items",items);
            }
        });
        $A.enqueueAction(action);
    }
    
})