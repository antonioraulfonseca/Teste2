@IsTest
public class TestVerifyDate {
	
    @isTest static void checkDatesTest(){
        VerifyDate.CheckDates(System.today().addDays(5), System.today().addDays(3));
        VerifyDate.CheckDates(System.today().addDays(30), System.today().addDays(30));
        VerifyDate.CheckDates(System.today().addDays(5), System.today().addDays(10));
    }
}