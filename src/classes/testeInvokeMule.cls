global class testeInvokeMule {
    
    webservice static List<String> testeInvoke(){
		List<String> contas = new List<String>();
        for(Account conta : [Select Id,Name From Account Limit 10]){
            contas.add(conta.Name);
        }
        return contas;
    }

}