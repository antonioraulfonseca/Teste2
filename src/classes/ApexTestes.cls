public class ApexTestes {
   
    private Account acc;
    public List<Account> listAccount { get; set; }
    
    public ApexTestes(ApexPages.StandardController controller){
        this.acc = (Account) controller.getRecord();
        listAccount = teste();
    }
    
    public List<Account> teste(){
        return [Select Name From Account];
        
    }
    
    public PageReference saveAccount(){
        insert this.acc;
        
        PageReference pageRef = new PageReference('/apex/ApexTestPage');
        pageRef.setRedirect(true);
		return pageRef;
        //return new PageReference('/'+this.acc.Id);
    }
}