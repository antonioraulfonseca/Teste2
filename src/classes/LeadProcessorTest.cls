@isTest
public class LeadProcessorTest {
    public static testMethod void myUnitTest(){
        List<Lead> listLead = new List<Lead>();
        Lead leadInstance;
        for(Integer i=0; i < 200; i++){
            leadInstance = new Lead();
            leadInstance.LastName = 'Teste ' + i;
            leadInstance.Company = 'Acc ' + i;
            leadInstance.Status = 'Open - Not Contacted';
            
            listLead.add(leadInstance);
        }
        Database.insert(listLead);
       
        System.Test.startTest();
        LeadProcessor processor = new LeadProcessor();
        Database.executeBatch(processor);
        System.Test.stopTest();
        
        System.assertEquals(200,[Select Count() From Lead Where LeadSource = 'Dreamforce']);
    }
}