@isTest
public class DailyLeadProcessorTest {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    public static testMethod void myUnitTest(){
        List<Lead> listLead = new List<Lead>();
        Lead leadInstance;
        for(Integer i=0; i < 200; i++){
            leadInstance = new Lead();
            leadInstance.LastName = 'Teste ' + i;
            leadInstance.Company = 'Acc ' + i;
            leadInstance.Status = 'Open - Not Contacted';
            
            listLead.add(leadInstance);
        }
        Database.insert(listLead);
        System.Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',CRON_EXP, new DailyLeadProcessor());
        System.Test.stopTest();
    }
}