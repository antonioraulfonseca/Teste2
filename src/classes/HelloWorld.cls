public class HelloWorld {
    
    private Account conta;
    public List<Account> returnAccounts{get;set;}
    public String contaNome{get;set;}
    
    
    public HelloWorld(ApexPages.StandardController controller){
        returnAccounts = [Select Name From Account Limit 20];
        conta = (Account) controller.getRecord();
    }
   	
    public void changeAccount(){
        List<Id> setIds = new List<Id>();
        for(Account c : [Select Id From Account Limit 2]){
            setIds.add(c.Id);
        }
        returnAccounts = [Select Name From Account Where Id=: setIds];
    }
}