public class RandomContactFactory {
    
    public static List<Contact> generateRandomContacts(Integer param1, String param2){
        List<Contact> cList = new List<Contact>();
        for(Integer i=0;i<param1;i++){
        	Contact c = new Contact();
            c.FirstName = 'Teste ' + i;
            c.LastName = param2;
            cList.add(c);
        }
        return cList;
    }

}