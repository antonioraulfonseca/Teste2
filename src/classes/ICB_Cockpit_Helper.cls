/**********************************************************************
Name: ICB_Cockpit_Helper
Copyright © 2016 Salesforce
======================================================
======================================================
Purpose:
Helper class for controller ICB_Cockpit
======================================================
======================================================
History
VERSION AUTHOR          DATE        DETAIL          Description
1.0     Antonio Raul    27/08/2016  Class creation 
1.0     Antonio Raul    28/08/2016  Class finished
1.1     Antonio Raul    30/08/2016  Class updated
***********************************************************************/
public with sharing class ICB_Cockpit_Helper 
{
   
}