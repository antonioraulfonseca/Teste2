@isTest
public class ApexTestes_test {
    testMethod static void metodo(){
        List<Account> listAccount = instancesTest.instanceAccount();
		System.Test.startTest();
		ApexTestes app = new ApexTestes();
        app.teste(listAccount);
        System.assertEquals('Teste',[Select Name From Account Order By CreatedDate Desc limit 1].Name);
		System.Test.stopTest();        
    }
    testMethod static void metodoError(){
        System.Test.startTest();
		ApexTestes app = new ApexTestes();
        app.teste(null);
		System.Test.stopTest();        
    }
  
}