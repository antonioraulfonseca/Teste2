@isTest
public class DevolveProdutosTest {
	
    static testMethod void devolve(){
        
        Fatura__c fat = [Select Quantidade_de_Produtos__c, Produto__r.ID from Fatura__c
                    where Name =: 'FAT-0001' Limit 1];   
        
        Produto__c prod = [Select Quantidade_em_Estoque__c from Produto__c
                          where ID =: fat.Produto__r.ID];
        
        prod.Quantidade_em_Estoque__c += fat.Quantidade_de_Produtos__c;
        update prod;
    }
}