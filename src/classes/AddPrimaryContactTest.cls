@isTest
public class AddPrimaryContactTest {

    public static testMethod void myUnitTest(){
        Account acc;
        List<Account> listAccount = new List<Account>();
        for(Integer i=0; i < 50; i++){
            acc = new Account();
            acc.Name = 'Teste' + i;
            acc.BillingState = 'NY';
            listAccount.add(acc);
        }
        Database.insert(listAccount);
        
        Contact contato = new Contact(LastName = 'Teste');
        Database.insert(new List<Contact>{contato});
        
        System.Test.startTest();
        System.enqueueJob(new AddPrimaryContact(contato,'NY'));
        System.Test.stopTest();
        
        
    }
}