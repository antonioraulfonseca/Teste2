public class SOQL {
    
    public static void soql(){
        
        String fat = [Select Name from Fatura__c][1].Name;
        
        System.debug(fat);
    }
    
    public static void soql2(){
        
        /*Account a = new Account(Name='Ralf');
        insert a;
        
        Contact c = new Contact(LastName='Antony');
        c.AccountId = a.Id;
        
        insert c;
        
        c = [Select Account.Name from Contact where ID =: c.Id Limit 1];
        
        System.debug(c.Account.Name);*/
        
        Fornecedor__c fon = new Fornecedor__c(Name='TesteF', CNPJ__c='00123254789');
        insert fon;
        
        Produto__c prod = new Produto__c(Name='Teste', Quantidade_em_Estoque__c=45, Pre_o__c=500,Marca__c='dell');
        prod.Fornecedor__c = fon.Id;
        insert prod;
        
    }
    
    public static void soql3(){
        
        String teste = [Select toLabel(Status__c) from Fatura__c][1].Status__c;//Pega o valor do status da fatura
        //só funciona com picklist
            
        System.debug(teste);
    }
    
    public static void sosl(){
        
    }
    
    

}