global with sharing class DevolveProdutos {
	
    webservice static String devolver(String nome){
    	
   		 Fatura__c fat = [Select Name, Quantidade_de_Produtos__c, Produto__r.ID from 
                          Fatura__c where Name = :nome];   
        
        Produto__c prod = [Select Quantidade_em_Estoque__c from Produto__c
                          where ID =: fat.Produto__r.ID];
        
        prod.Quantidade_em_Estoque__c += fat.Quantidade_de_Produtos__c;
        update prod;
        delete fat;
        
        return 'Fatura excluida com sucesso';
    }
    
}