public class AddPrimaryContact implements Queueable{
	
    private Contact cont;
    private String state;
    
    public AddPrimaryContact(Contact contato, String state){
        this.cont = contato;
        this.state = state;
    }
    
    public void execute(QueueableContext context) {
        for(Account acc : [Select Id From Account Where BillingState =: this.state limit 200 ]){
            Contact contato = this.cont.clone();
            contato.AccountId = acc.Id;
        }
    }
}