public class Case_TriggerHandler {
    
    public static void sendEmail(List<Case> newCaseList){
        List<Contact> sendTo = new List<Contact>();
        Set<Id> setIdCases = new Set<Id>();
        for(Case nCase : newCaseList){
            setIdCases.add(nCase.Id);
        }
        for(Case_Collaborator__c caseCollaborator : retrieveCollaborators(setIdCases)){
            sendTo.add(caseCollaborator.Contacts__r);
        }
        Messaging.sendEmail(buildEmail(sendTo)) ;
    }
    private static List<Messaging.SingleEmailMessage> buildEmail(List<Contact> sendTo){
        List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
        Id idTemplate = [Select Id From EmailTemplate Where DeveloperName = 'Embraer'].Id;
        for(Contact send : sendTo){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List<String>{send.Email});
            mail.setTargetObjectId(send.id);
            mail.setSaveAsActivity(false);
            mail.setReplyTo('antonio.raul.fonseca@outlook.com');
            mail.setSenderDisplayName('Embraer');
            mail.setCcAddresses(new List<String>{'antonio.raul.fonseca@outlook.com'});
            mail.setTemplateId(idTemplate);
            listMail.add(mail);
        }
        
        return listMail;
    }
    private static List<Case_Collaborator__c> retrieveCollaborators(Set<Id> idsCases){
        return [SELECT Contacts__r.Email FROM Case_Collaborator__c WHERE Cases__c =: idsCases];
    }
}