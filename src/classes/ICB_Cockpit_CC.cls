/**********************************************************************
Name: ICB_Cockpit_CC
Copyright © 2016 Unilever
======================================================
======================================================
Purpose:
Controller class for lightning component ICB_Cockpit And ICB_CloseSale
======================================================
======================================================
History
VERSION AUTHOR          DATE        DETAIL          Description
1.0     Antonio Raul    09/08/2016  Class creation 
1.0     Antonio Raul    19/08/2016  Class finished 
2.0     Antonio Raul    20/08/2016  Class updated
2.0     Antonio Raul    25/08/2016  Finished updated
***********************************************************************/
public with sharing class ICB_Cockpit_CC 
{    
    
    
}