public class ControllerCaseNew {
	
    private Case caseNew;
    private Map<Id,Boolean> mapSelected = new Map<Id,Boolean>();
    private Set<Id> setSelected = new Set<Id>();
    public List<ItemContact> listItems { get; set;}
    public List<ItemContact> listItemsSelected { get; set;}
    
    public Integer quantityPerPage;
    public Integer valueOffset;
    public Integer currentPage { get;set; }
    
    public ControllerCaseNew(ApexPages.StandardController controller){
        this.caseNew = (Case)controller.getRecord();
        initValuesPagination();
    }
    
    public void contactList(){
        if(this.caseNew.AccountId != null){
            listItems = new List<ItemContact>();
            for(Contact cont : [Select Id, Name From Contact Where AccountId =: this.caseNew.AccountId AND Id <>: setSelected Limit :quantityPerPage OFFSET :valueOffset]){
                ItemContact item = new ItemContact(cont);
                item.isSelected = mapSelected.get(cont.Id);
                listItems.add(item);
            }
        }else{
            listItems.clear();
        }
    }
    public void addContact(){
        if(listItemsSelected == null) listItemsSelected = new List<ItemContact>();
        for(Integer i = 0 ; i < listItems.size(); i++){
            if(listItems.get(i).isSelected){
                setSelected.add(listItems.get(i).itemContact.Id);
                listItemsSelected.add(listItems.get(i));
                listItems.remove(i);
                addContact();
                contactList();
            }
        }
    }
    public void populateMap(){
        for(ItemContact item : listItems){
            if(item.isSelected){
            	mapSelected.put(item.itemContact.Id, item.isSelected);    
            }else{
                mapSelected.remove(item.itemContact.Id);
            }
        }
    }
    public PageReference saveCaseCollaborator(){
        try{
            Database.insert(this.caseNew);
            List<Case_Collaborator__c> collaboratorsList = new List<Case_Collaborator__c>();
            Case_Collaborator__c collaborator;
            for(ItemContact item : listItemsSelected){
                collaborator = new Case_Collaborator__c();
                collaborator.Contacts__c = item.itemContact.Id;
                collaborator.Cases__c = this.caseNew.Id;
                collaboratorsList.add(collaborator);
            }
            Database.insert(collaboratorsList);
            return new PageReference('/'+this.caseNew.Id);
        }catch(Exception e){
            System.debug('Exception = ' + e.getMessage());
            
        }
       return null;
    }
    
    public void initValuesPagination(){
        
        this.quantityPerPage = 5;
        this.valueOffset = 0;
        this.currentPage = 1;
    }
    
    public void nextPage(){
        
        this.valueOffset = valueOffset + this.quantityPerPage;
        contactList();
        this.currentPage++;
        if(this.listItems.size() == 0){
            previousPage();
        }
        
    }
    
    public void previousPage(){
        
        if(this.valueOffset >= this.quantityPerPage){
            this.valueOffset = this.valueOffset - this.quantityPerPage;
            contactList();
            this.currentPage--;
        }
        
    }
    
    
    public class ItemContact{
        public Boolean isSelected { get; set; }
        public Contact itemContact { get; set; }
        
        public ItemContact(){}
        public ItemContact(Contact contactItem){
            this.itemContact = contactItem;
        }
        
    }
    
    
}