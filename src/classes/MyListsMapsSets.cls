@isTest
public class MyListsMapsSets {
    
    static testMethod void lists(){
        
        List<String> collors = new List<String>();
    
        collors.add('Blue');
        collors.add('Yellow');
        collors.add('Red');
        
        for(String s : collors){
            System.debug(s);
        }
    
    }
        
    static testMethod void lists2(){
        List<Integer> numbers = new List<Integer>{
            1,2,3,4,5,6,7,8,9,0
        };
        
            numbers.sort();
        System.assertEquals(1, numbers.get(1));
        System.assertEquals(2, numbers.get(2));
        System.assertEquals(3, numbers.get(3));
        System.assertEquals(4, numbers.get(4));
        System.assertEquals(5, numbers.get(5));
        System.assertEquals(6, numbers.get(6));
        System.assertEquals(7, numbers.get(7));
        System.assertEquals(8, numbers.get(8));
        System.assertEquals(9, numbers.get(9));
        System.assertEquals(0, numbers.get(0));
        
    }
    
    static testMethod void lists3(){
        
        List<SelectOption> options = new LIST<SelectOption>();
        
        options.add(new SelectOption('A', 'Mexico'));
        options.add(new SelectOption ('D', 'USA'));
        options.add(new SelectOption('B', 'Brazil'));
        options.add(new SelectOption('C', 'Argentina'));
        System.debug('Before sort Options: ' + options);
        options.sort();
        System.debug('After sort Options: ' + options);
        
    }
}