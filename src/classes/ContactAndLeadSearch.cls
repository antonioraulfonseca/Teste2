public class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String nome){
        return [FIND :nome IN ALL FIELDS 
                RETURNING
               Contact(LastName), Lead(LastName)];
    }

}