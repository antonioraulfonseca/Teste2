global class LeadProcessor implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext bc){
        String queryLocator = 'Select Id,LeadSource  From Lead';
        return Database.getQueryLocator(queryLocator);
    }
    global void execute(Database.BatchableContext bc, List<Lead> scope){
        List<Lead> listLead = new List<Lead>();
        for(Lead l : scope){
            l.LeadSource = 'Dreamforce';
            listLead.add(l);
        }
        Database.update(listLead);
    }
    global void finish(Database.BatchableContext bc){}
    
}