public class AccountProcessor {

    @future
    public static void countContacts(List<Id> accountIds){
        List<Account> listAccount = new List<Account>();
        for(Account acc : [Select (Select Id from Contacts) from Account Where Id=:accountIds]){
            acc.Number_of_Contacts__c = acc.Contacts.size();
            listAccount.add(acc);
        }
        Database.update(listAccount);
    }
}