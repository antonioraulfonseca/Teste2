public class teste {
	static Account[] accs= new List<Account>(); 
    
    //Inserindo Valores
    public static void testeMethod(){
    	for(Integer i=0; i < 3; i++){
    		Account a = new Account(Name='Acme' + i, Phone='98734025');  
            accs.add(a);
    	}
        insert accs;
    }
    //atualizando valores
    public static void testeMethodUp(String nome, String phone){
        Account ab = [Select Name from Account where Name =: nome Limit 1];
        ab.Name = 'Raul';
        ab.Phone = phone;
       	update ab;
    }
    //inserindo e atualizando ao mesmo tempo
    public static void testeMethodUpS(){
        Account[] abs = [Select Name from Account where Name like '%Acme%'];
        
        for(Account a : abs){
            a.Name = 'Mudando';
        }
        
        Account x = new Account(Name = 'Luiza');
        
        abs.add(x);
        
        upsert abs;
    }
    
    //deletando
    public static void testeMethodDel(){
        Account[] a = [Select Name from Account where Name = 'Luiza' all rows];
        //para o undelete, para o delete não precisa do all rows
        
        delete a;
        undelete a;//restaura os deletados
    }
}