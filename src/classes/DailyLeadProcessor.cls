global class DailyLeadProcessor implements Schedulable{

    global void execute(SchedulableContext ctx) {
        List<Lead> listLead = new List<Lead>();
        for(Lead l : [Select Id,LeadSource from Lead Where LeadSource = null]){
            l.LeadSource = 'Dreamforce';
            listLead.add(l);
        }
        Database.update(listLead);
    }
}