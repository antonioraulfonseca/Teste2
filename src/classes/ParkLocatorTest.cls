@isTest
public class ParkLocatorTest {

    @isTest static void myUnitTest(){
        System.Test.setMock(WebServiceMock.class, new ParkServiceMock());
        List<String> parks = ParkLocator.country('Brasil');
        System.assertEquals(2, parks.size());
        System.assertEquals('Cajá', parks[0]);
		System.assertEquals('senzala', parks[1]);        
    }
}