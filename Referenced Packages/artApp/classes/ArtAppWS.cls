/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ArtAppWS {
    global ArtAppWS() {

    }
    webService static String getArtworkExpense(String myParentId, String myObject) {
        return null;
    }
    webService static String getCollectionExpense(String myParentId, String myObject) {
        return null;
    }
    webService static String getExhibitionExpense(String myParentId, String myObject) {
        return null;
    }
    webService static String getId(String myId, String myObject) {
        return null;
    }
    webService static String getLineItem(String myParentId, String myObject) {
        return null;
    }
    webService static String getPreFix(String myObjectName) {
        return null;
    }
    webService static String getRtId(String myType) {
        return null;
    }
    webService static String getRtIdByObj(String myType, String myObj) {
        return null;
    }
    webService static String getTransactionClone(String myOriginalId, String newRT) {
        return null;
    }
    webService static String getTransactionExpense(String myParentId, String myObject) {
        return null;
    }
}
