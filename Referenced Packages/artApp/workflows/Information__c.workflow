<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetInformation</fullName>
        <description>Sets the Information Summary.</description>
        <field>Information__c</field>
        <formula>Information1__c + Information2__c</formula>
        <name>SetInformation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSynopsis</fullName>
        <description>Sets the Information Synopsis.</description>
        <field>Synopsis__c</field>
        <formula>LEFT(Information1__c + Information2__c, 254)</formula>
        <name>SetSynopsis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SetInformation</fullName>
        <actions>
            <name>SetInformation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetSynopsis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Information Summary and Synopsis.</description>
        <formula>NOT(ISBLANK( Id ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
