<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetTransactionAddress1</fullName>
        <field>Address_1__c</field>
        <formula>IF( Inbound__c ,  Artcenter__r.Address_1__c  ,
IF(NOT(ISBLANK( Artist__c )), Artist__r.Address_1__c,
IF(NOT(ISBLANK( Collector__c )), Collector__r.Address_1__c,
Transfer_Artcenter__r.Address_1__c 
)))</formula>
        <name>SetTransactionAddress1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionAddress2</fullName>
        <field>Address_2__c</field>
        <formula>IF( Inbound__c , Artcenter__r.Address_2__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.Address_2__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.Address_2__c, 
Transfer_Artcenter__r.Address_2__c 
)))</formula>
        <name>SetTransactionAddress2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionAddressee</fullName>
        <field>Addressee__c</field>
        <formula>IF( Inbound__c , Artcenter__r.Name , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.Full_Name__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.Full_Name__c, 
Transfer_Artcenter__r.Name 
)))</formula>
        <name>SetTransactionAddressee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionCity</fullName>
        <field>City__c</field>
        <formula>IF( Inbound__c , Artcenter__r.City__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.City__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.City__c, 
Transfer_Artcenter__r.City__c 
)))</formula>
        <name>SetTransactionCity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionEmail</fullName>
        <field>Addressee_Email__c</field>
        <formula>IF( Inbound__c , Artcenter__r.Email__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.Email__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.Email__c, 
Transfer_Artcenter__r.Email__c 
)))</formula>
        <name>SetTransactionEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionPhone</fullName>
        <field>Phone__c</field>
        <formula>IF( Inbound__c , Artcenter__r.Phone__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.Phone__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.Phone__c, 
Transfer_Artcenter__r.Phone__c 
)))</formula>
        <name>SetTransactionPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionStateProvince</fullName>
        <field>State_Province__c</field>
        <formula>IF( Inbound__c , Artcenter__r.State_Province__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.State_Province__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.State_Province__c, 
Transfer_Artcenter__r.State_Province__c 
)))</formula>
        <name>SetTransactionStateProvince</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetTransactionZipPostalCode</fullName>
        <field>Zip_Postal_Code__c</field>
        <formula>IF( Inbound__c , Artcenter__r.Zip_Postal_Code__c , 
IF(NOT(ISBLANK( Artist__c )), Artist__r.Zip_Postal_Code__c, 
IF(NOT(ISBLANK( Collector__c )), Collector__r.Zip_Postal_Code__c, 
Transfer_Artcenter__r.Zip_Postal_Code__c 
)))</formula>
        <name>SetTransactionZipPostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SetTransactionAddress1</fullName>
        <actions>
            <name>SetTransactionAddress1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Address_1__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionAddress2</fullName>
        <actions>
            <name>SetTransactionAddress2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Address_2__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionAddressee</fullName>
        <actions>
            <name>SetTransactionAddressee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Addressee__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionCity</fullName>
        <actions>
            <name>SetTransactionCity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( City__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionEmail</fullName>
        <actions>
            <name>SetTransactionEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Addressee_Email__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionPhone</fullName>
        <actions>
            <name>SetTransactionPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Phone__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionStateProvince</fullName>
        <actions>
            <name>SetTransactionStateProvince</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( State_Province__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetTransactionZipPostalCode</fullName>
        <actions>
            <name>SetTransactionZipPostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Zip_Postal_Code__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
