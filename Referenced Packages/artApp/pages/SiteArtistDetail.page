<apex:page docType="html-5.0" controller="artApp.SiteArtistDetailCon" standardStylesheets="false" showHeader="false" sidebar="false">
<apex:composition template="artApp__PublicSiteTemplate"> 
    <apex:define name="head">
         
        <meta name="description" content="{!myRecord.Brief_Biography__c}" />
        <meta name="keywords" content="{!$Organization.Name} {!myRecord.Full_Name__c} " />
        <title>{!myRecord.Full_Name__c}</title>
        <meta property="og:title" content="{!myRecord.Full_Name__c}" />
        <meta property="og:description" content="{!myRecord.Brief_Biography__c}"/>
        <meta property="og:type" content="product" />
        <meta property="og:image" content="{!imageFilePath}{!myRecord.Primary_Image_Id__c}" />
        <meta property="og:site_name" content="{!$Organization.Name} site powered by ArtApp" />
        
    </apex:define>
       
    <apex:define name="body">
      <!--<div class="container">-->
      <div class="row">
        <div class="col-md-3">
            <apex:outputPanel rendered="{!Len(myRecord.artApp__Primary_Image_Id__c)>0}" layout="none">
                <img src="{!imageFilePath}{!myRecord.Primary_Image_Id__c}" class="img-thumbnail img-responsive"/> 
            </apex:outputPanel>
            <apex:outputPanel rendered="{!Len(myRecord.artApp__Primary_Image_Id__c)==0}" >
                <img data-src="holder.js/300x300/text:{!myRecord.Full_Name__c}" alt="{!myRecord.Full_Name__c}" class="img-thumbnail img-responsive"/>
            </apex:outputPanel>      
        </div>
        <div class="col-md-9">
          <h1><img src="/s.gif" alt="{!myRecord.Name}" class="iconArtist"/>{!recordLabel}: {!myRecord.Full_Name__c}</h1>
          <fieldset>
        <legend>Details</legend>
        <div class='row'>
            <apex:outputPanel rendered="{!IF(myRecord.Artcenter__r.artApp__Is_Public__c, true, false)}">
               <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class='form-group'>
                        <label>{!$ObjectType.Artwork__c.fields.Artcenter__c.Label}</label>
                        <p class="help-block">
                            <a href="{!URLFOR($Page.SiteArtcenterDetail)}?id={!myRecord.Artcenter__c}" title="{!myRecord.Artcenter_Name__c}">
                                    {!myRecord.Artcenter_Name__c}
                            </a>
                        </p>
                    </div>
                </div>   
            </apex:outputPanel>
            <apex:repeat value="{!$ObjectType.artApp__Artist__c.FieldSets.artApp__SiteDetailNarrow}" var="s">
                <apex:outputPanel rendered="{!IF(ISNull(myRecord[s]), false, true)}">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class='form-group'>
                        <label>{!s.Label}</label>
                        <p class="help-block"><apex:outputField value="{!myRecord[s]}"/></p>
                    </div>
                </div>
                </apex:outputPanel>
            </apex:repeat>
            <apex:outputPanel rendered="{!hasSocialMedia}">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class='form-group'>
                        <label>{!$Label.VF_Label_SiteDetail_FollowMe}</label>
                        <p class="help-block">
                            <apex:repeat value="{!$ObjectType.artApp__Artist__c.FieldSets.artApp__SiteDetailSocialMedia}" var="s">
                                <apex:outputPanel rendered="{!IF(ISNull(myRecord[s]), false, true)}">
                                  <apex:outputLink target="_blank" title="{!$Label.artapp__VF_Label_SiteDetail_FollowMe}{!s.Label}" value="{!myRecord[s]}" >
                                      <apex:image url="/s.gif" styleClass="{!IF(contains(s.FieldPath, "artApp__"), "", "artApp__")}{!s.FieldPath}Icon"/>
                                  </apex:outputLink>
                                </apex:outputPanel>
                            </apex:repeat>
                        </p>
                    </div>
                </div>
            </apex:outputPanel>
        </div>
    </fieldset>
    </div>
    </div>
    <br/>
    <fieldset>
        <legend>Description</legend>
        <div class="clearfix">
            <c:SiteSocialMedia />
            <br/>
        </div>       
        <div >
           <apex:repeat value="{!$ObjectType.artApp__Artist__c.FieldSets.artApp__SiteDetailWide}" var="s">
               <apex:outputPanel rendered="{!IF(ISNull(myRecord[s]), false, true)}">
                   <p><apex:outputField value="{!myRecord[s]}"/></p>
                </apex:outputPanel>
           </apex:repeat>
        </div>
    </fieldset>      
    <br/>
        
    
      <!--</div>-->
    <div class="panel-group" id="accordion">
          <apex:outputPanel rendered="{!hasChildRecords5}" layout="none" >
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      <h4><img src="/s.gif" alt="{!myChildRecordsLabelPlural5}" class="{!myChild24Icon5}"/>
                         {!myChildRecordsLabelPlural5} <span class="badge">{!myChildRecords5Set.collectionSize}</span>
                      </h4>
                    </a>
                  </h4>
                </div>
                <apex:form >
                 <apex:outputPanel id="recordPanel" >
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body">
                         <c:ListPaginator pController="{!myChildRecords5Set.controller}" panel="recordPanel"/>
                         <apex:repeat value="{!ChildRecords5}" var="item">
                             <div class="media">
                                  <div class="pull-left" href="">
                                      <a class="quiet" href="{!URLFOR($Page.artApp__SiteArtworkDetail)}?id={!item.id}" title="{!item.Name}">                               
                                        <apex:image id="theImage" width="150px" value="{!imageFilePath}{!item.artApp__Primary_Image_Id__c}"  rendered="{!Len(item.artApp__Primary_Image_Id__c)>0}" styleClass="img-responsive"/>
                                        <apex:panelGroup rendered="{!AND(Len(item.artApp__Primary_Image_Id__c)=0,Len(item.artApp__Primary_Image_URL__c)>0)}" >
                                            <img class="media-object" src="{!item.Primary_Image_URL__c}" alt="{!item.name}" width="150px"/>
                                        </apex:panelGroup>
                                        <apex:panelGroup rendered="{!IF(AND(item.artApp__Primary_Image_Id__c == null, item.artApp__Primary_Image_URL__c == null), true, false)}" >
                                            <img data-src="holder.js/150x100/text:{!item.Name}" alt="{!item.Name}" class="info"/>
                                        </apex:panelGroup>
                                      </a>
                                  </div> 
                              <div class="media-body">
                                    <h4 class="media-heading">{!item.Name} <small>{!item.RecordType.Name}</small></h4>
                                <fieldset>
                                <div class='row'>
                                    <apex:repeat value="{!$ObjectType.artApp__Artwork__c.FieldSets.artApp__SiteDetailChild}" var="s">
                                        <apex:outputPanel rendered="{!IF(ISNull(item[s]), false, true)}">
                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <div class='form-group'>
                                                <label>{!s.Label}</label>
                                                <p class="help-block"><apex:outputField value="{!item[s]}"/></p>
                                            </div>
                                        </div>
                                        </apex:outputPanel>
                                    </apex:repeat>
                                </div>
                                </fieldset>
                                <hr/>
                              </div>
                            </div>
                         </apex:repeat>
                      </div>
                      <apex:panelGroup rendered="{!hasMoreChildRecords5}" layout="none">
                          <div class="panel-footer">
                            <a href="{!myChildRecordMoreUrl5}" title="{!$Label.VF_Label_SiteDetail_ShowMore} {!myChildRecordsLabelPlural5}" class="btn btn-info btn-xs">                                  
                                {!$Label.VF_Label_SiteDetail_ShowMore} {!myChildRecordsLabelPlural5}
                            </a>
                          </div>
                      </apex:panelGroup>
                    </div>
                    <script type="text/javascript" > 
                        Holder.run({images: ".info"});
                    </script>
                 </apex:outputPanel>
                </apex:form>
              </div>
          </apex:outputPanel>
    </div>   
       
    
       
           
    </apex:define>
    </apex:composition>  
</apex:page>